var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: true });


nightmare
  .goto('http://pre.mp3.zing.vn')
  .type('.section-search input', 'Noi nay co anh')
  .click('.section-search .input-btn button')
  .wait('#song-search-rs')
  .click('#song-search-rs .item-song:first-child a ')
  .evaluate(function () {
    return document.querySelector('#zero_click_wrapper .c-info__title a').href;
  })
  .end()
  .then(function (result) {
    console.log(result);
  })
  .catch(function (error) {
    console.error('Search failed:', error);
  });