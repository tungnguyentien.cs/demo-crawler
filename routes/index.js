var express = require('express');
var router = express.Router();
var crawler = require('../crawler');

/* GET home page. */
router.post('/', function(req, res, next) {
	var data = {
		url: req.body.url,
		parser: req.body.parser
	};
  	crawler.get(data, function(err, result){
  		if (err || !result) return res.send(JSON.stringify({
  			err: JSON.stringify(err),
  			data: ''
  		}));

  		var json = {
  			err: 0,
  			data: result
  		};
  		res.send(JSON.stringify(json));
  	})
});

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index',{title: 'Demo Crawler'});
});
module.exports = router;
