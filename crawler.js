var cheerio = require('cheerio');
var request = require('request');
var _ = require('lodash');
var async = require('async');
var base64 = require('base-64');
var utf8 = require('utf8');

var crawler = {};
var _task = [];

var _getLink = function(context, regex) {
	var $ = context;
	var match = new RegExp(regex);
	$('body a').each(function(index, elemnent) {
		var link = $(elemnent).attr('href');
		if(_.find(_task, link)) return;

		if (regex != null && match.test(link)) _task.push(link);
		else if (regex == null) _task.push(link);
	});
}

var _getHtml = function(url, callback) {
	return request({
		method: 'POST',
		uri: url,
		headers: {
			'Accept': 'application/json',
        	'Accept-Charset': 'utf-8',
        	'Content-Type': 'text/html;UTF-8;charset=utf-8'
		}
	}, function(error, response, body) {
		if (error) return callback && callback(error, null);
		return callback && callback(null, body);
	})
}


var _run = function() {
	var url = 'http://news.zing.vn';
	console.log('Start crawl ' + url);
	_getHtml(url, function(err, result) {
		if (err || !result) return console.log("Error: " + err);
		var context = cheerio.load(result);
		var regex = '.*post[0-9]+.html';
		_getLink(context, regex);

		var json = [];
		var crawlData = function(link, callback) {
			_getHtml(url + link, function(err, result){
				if (err || !result) return callback();
				var context = cheerio.load(result);
				var data = pageFunction(context);
				json.push(data);
				return callback();
			})
		}
		async.map(_task, crawlData, function(err, data){
			if (err) return console.log(err);
		})
	})
}

var _decodeBase64 = function(content) {
	var bytes = base64.decode(content);
	return utf8.encode(bytes);
}
module.exports = {
	get: function(data, callback) {
		async.waterfall([
			function(cb) {
				_getHtml(data.url, function(err, result) {
					if (err || !result) return cb(err, null);
					return cb(null, result);
				})
			},
			function (html, cb) {
				try {
					var context = cheerio.load(html);
					var parserFunc = _decodeBase64(data.parser);
					eval(parserFunc);
					var result = pageFunction && pageFunction(context);
				} catch(err) {
					console.log(err);
					return cb(err, null);
				}
				return cb(null, result);
			}
		], function(err, result){
			if (err || !result) return callback && callback(err, null);
			return callback && callback(null, result)
		})
	}
}